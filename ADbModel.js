"use strict";

const Promise = require("bluebird");

module.exports = class AModel {
    constructor (core, settings) {
        this._core = core;
        this._settings = settings;
    }

    get core () {
        return this._core;
    }

    get log () {
        return this.core.log;
    }

    get settings () {
        return this._settings;
    }

    get modules () {
        return this.core.modules;
    }

    get db () {
        return this._core.modules.get("anna-postgres").db;
    }

    get pgp () {
        return this._core.modules.get("anna-postgres").pgp;
    }

    get pkField () {
        return "id";
    }

    get schema () {
        return "public";
    }

    get table () {
        throw new Error("AModel.table property is virtual and should me overriden!");
    }

    get fullTableName () {
        return `${this.schema}.${this.table}`;
    }

    sql(file) {
        return new this.pgp.QueryFile(file, { debug: true, minify: true });
    }

    // TODO: !!! Will need to refactor this to eliminate module name hardcoding.
    // Maybe this should be a port of the core rather than modules system.
    getModel (model) {
        return this.core.modules.get("anna-postgres").models.get(model);
    }

    normalizeFieldList (fieldList) {
        const test = !!fieldList && Array.isArray(fieldList) && fieldList.length > 0;
        const fields = test ? fieldList.join(",") : "*";
        return fields;
    }

    findById (id, fieldList) {
        const fields = this.normalizeFieldList(fieldList);
        const query = `SELECT ${fields} FROM ${this.fullTableName} WHERE ${this.pkField}=$1`;
        return this.db.oneOrNone(query, [id]);
    }

    findAll (fieldList) {
        const fields = this.normalizeFieldList(fieldList);
        const query = `SELECT ${fields} FROM ${this.fullTableName}`;
        return this.db.any(query);
    }
    
    updateById (id, update) {
        let query = `UPDATE ${this.fullTableName} SET `;
        Object.keys(update).forEach(key => {
            query += `${key} = `+"${"+key+"},";
        });
        query = query.slice(0, -1);
        query += ` WHERE ${this.pkField}=`+"${id}";
        update.id = id;
        return this.db.query(query, update);
    }
}
