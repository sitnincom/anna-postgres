"use strict";

const Promise = require("bluebird");
const AModule = require("anna/AModule");

module.exports = class PostgresModule extends AModule {
    constructor(core, settings) {
        super(core, settings);

        const pgpOpts = {
            promiseLib: Promise
        };

        if (this.core.debug) {
            pgpOpts.query = (event) => {
                this.log.debug("--- PG QUERY:", event.query);
            };

            pgpOpts.error = (err, event) => {
                this.log.debug("--- PG ERROR:", err, event.query);
            }
        }

        this._pgp = require("pg-promise")(pgpOpts);

        this._db = null;
        this._models = new Map();
    }

    get pgp() {
        return this._pgp;
    }

    get db() {
        if (!this._db) {
            this._db = this.pgp(this.settings.connection);
        }

        return this._db;
    }

    get models() {
        return this._models;
    }

    registerModels(models) {
        Object.keys(models).forEach(name => {
            const model = new models[name](this.core);
            this.models.set(name, model);
        });
    }

    init() {
        this.log.debug(`${this.constructor.name} module init()`);
        return this.db.query("SELECT 1+1");
    }
}
